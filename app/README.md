# Warp Core React Frontend

Warp Core's frontend is based on a Create-React-App in TypeScript, and uses a Redux-like state store using Context.

## State Store

The app is wrapped in multiple providers, each handling its own layer of the state. There's a theme provider to handle the CSS styles, a contract provider to handle smart contract logic, interfaces, and artifacts, a store provider to handle the app-specific global state, and a web3react provider to handle wallet connections.

TODO: scaffold out other providers, describe

## Web3React

Warp Core connects the app to the user's wallet using the [`web3react`](https://github.com/NoahZinsmeister/web3-react) library. We've written some extensive documentation for it, and will provide a table of contents for easier reference:

Table of Contents
* [Wallet and Provider Connectors](#Wallet-and-Provider-Connectors)
* [Connecting Wallets](#Connecting-Wallets)
* [Interacting with Wallets and Providers](#Interacting-with-Wallets-and-Providers)
* [Web3React API Reference](#Web3React-API-Reference)
  * [`connector`](#connector)
  * [`account`](#account)
  * [`activate`](#activate)
  * [`active`](#active)
  * [`chainId`](#chainId)
  * [`deactivate`](#deactivate)
  * [`error`](#error)
  * [`provider`](#provider)
  * [`setError`](#setError)
  * [Custom Functions](#Custom-Functions)
* [Connecting with the Wallet](#Connecting-with-the-Wallet)
* [Disconnecting a Wallet](#Disconnecting-a-Wallet)
* [Custom Warp Core Wallet UI Flags](#Custom-Warp-Core-Wallet-UI-Flags)
* [Additional Utilities](#Additional-Utilities)

### Wallet and Provider Connectors

Wallets and web3 providers (such as Infura) connect to an app in `web3react` via a `connector`. The connectors are listed in `src/wallets/connectors.ts`. Currently there is support for injected wallets such as MetaMask, providers such as Infura, widget wallets such as Portis, and more. If you wish to add a new wallet or provider, there is likely already a web3react package to aid the process (see Portis for an example). There is a list of packages on the landing page of the GitHub repo ([here](https://github.com/NoahZinsmeister/web3-react)).

### Connecting Wallets

Wallets are connected in `src/containers/WalletWrapper.tsx`. As of this writing, Warp Core ships with support for Portis and Torus, along with support for injected providers (such as MetaMask).

We'll describe the process of connecting a wallet using `web3-react`, and also use WalletConnect as an example with code snippets.

First you'll need to get the connector for the desired wallet from the `web3-react` repo. You can find the code for the connectors [here](https://github.com/NoahZinsmeister/web3-react/tree/v6/packages), though each is also its own npm package, and should be added using Yarn, installing each separately as needed.

If we wanted WalletConnect, we would get the package using `yarn add @web3-react/walletconnect-connector`.

Each wallet needs its own unique instantiation details. We won't give a guide for each wallet here other than giving our WalletConnect example. If you want to add something else, head over to the package details in the `web3-react` repo and see what you'll need there.

With WalletConnect, we'd head to the [`walletconnect-connector` directory](https://github.com/NoahZinsmeister/web3-react/tree/v6/packages/walletconnect-connector). In the `src/` directory there we can see an `index.ts` file, which has a TypeScript interface called `WalletConnectConstructorArguments`. These are the arguments you'll need when creating a WalletConnect connector inside your app. Well, there's actually only one for WalletConnect, but many others have more.

```typescript
export interface WalletConnectConnectorArguments extends IWalletConnectProviderOptions {
  supportedChainIds?: number[]
}
```
Warp Core provides `wallet/connectors.ts` for wallet connections. To connect WalletConnect, we'd import the WalletConnect connector:
```typescript
// in wallets/connectors.ts
import { WalletConnectConnector } from '@web3-react/walletconnect-connector'
```
Using the interface as our guide we can see that the only obligatory argument is `chainId`. If we're testing on Goerli, this means all we'd need to add to `connectors.ts` is:
```typescript
export const walletConnect = new WalletConnectConnector({ chainId: 5 });
```
(`chainId: 5` is Goerli, you can replace that with whatever chain you want to work with.)

Next, import your wallet in `containers/WalletWrapper.tsx`. Currently line 6 is:
```typescript
import { injectedConnector, portis, torus } from "../wallets/connectors";
```
You can just add `walletConnect` to the list:
```typescript
import { injectedConnector, portis, torus, walletConnect } from "../wallets/connectors";
```

_The following details how to integrate a new wallet in `connectors.tsx`. If you are already comfortable with web3/blockchain in React, you can probably skip this section and infer it directly from the code and existing examples there._

You'll need to create a new function for selecting the wallet you've integrated. You can see the `selectMetamask` and `selectPortis` functions in `connectors.tsx`, if we wanted to add WalletConnect, we'd add:
```typescript
const selectWalletConnect = () => {
    setWallet(walletConnect);
  };
```
Selecting does not mean activating the wallet. It just puts it in line to be activated. The UI analog is when the user has selected a wallet on the menu, but has not confirmed the choice.

In order for a button to show up in a "select wallet" modal, you'll also need to add an object to the `wallets` array.
```typescript
const wallets = [
    // ...Metamask, Portis, and Torus objects
    {
      name: "walletConnect",
      connectFunction: selectWalletConnect,
      selected: wallet === walletConnect,
      activating: activatingConnector === walletConnect,
      active: connector === walletConnect,
      icon: WalletConnect,
    },
]
```
(If you want the icon, you'll need to find it. It should be an svg, and placed in `/components/icons`, then imported as a React component. If you're following along with WalletConnect, they have a repo of assets in their GitHub, and [this](https://github.com/WalletConnect/walletconnect-assets/blob/master/svg/walletconnect-logo.svg) is probably what you're looking for.)

Many of these properties (`selected`, `activated`, and more) are provided to help make for easy UI patterns which will be discussed and documented later.

The `handleConnect` function is what actually connects the wallet to the app. Part of the beauty of `web3-react` and Warp Core is that the `handleConnect` doesn't need to be changed or updated at all. We'll cover the `handleConnect` function in a bit more detail further on, after discussing the `web3-react` API.

You now have a new wallet integrated!

We're going to first talk about the `web3react` API to give you an overview of what's available using `web3react`, followed by dedicated sections for connecting and disconneting wallets that you've integrated into your dapps.

### Interacting With Wallets and Providers

Once you have a connected provider and/or wallet, `web3react` gives you a number of modules for interacting with the wallet. In any component interacting with the wallet, first import `useWeb3React`:
```typescript
import { useWeb3React } from "@web3-react/core";
```
Then destructure the relevant modules from `web3react` like so:
```typescript
const {
    account,
    activate,
    active,
    chainId,
    connector,
    deactivate,
    error,
    provider,
    setError,
} = useWeb3React();
```
We'll give an explanantion of the most common and useful facets of the `webreact` library that we're aware of.

#### `connector`

This is the abstract class for wallets - each wallet connector inherits from this class. Once you've connected a wallet using the steps above, it becomes the connector. This is only available once the wallet has been connected using the steps above and is the active wallet.

A number of the functions that `connector` exposes are also available directly through `useWeb3React()`, even without using `connector` directly. The rest of the functions that `connector` expose can change from wallet to wallet - not everything here will necessarily be implemented in every wallet, and the functions might behave differently from wallet to wallet. One particularly pertinent example is the `connector.close()` functions, which removes a wallet from the DOM. This can mean needing various checks when calling these functions, such as a `try/catch` or an `if` checking which wallet is connected (`if connector === walletConnect`, for example). We'll discuss disconnecting wallets in more detail later.

If a `supportedChainIds` array was provided when the wallet was instantiated (highly recommended), it is also available through `connector.supportedChainIds`.

In addition, there are a number of outputs sent to the console from `connector`. You can see these in the abstract class [here](https://github.com/NoahZinsmeister/web3-react/blob/v6/packages/abstract-connector/src/index.ts).

#### `account`

_(Also accessible as `connector.getAccount()`)_

This is the address of the connected wallet - anytime you need the user's address, you can use `account`.
```typescript
const displayAddress = () => (
  <div>Your address is: {account}</div>
)
```
#### `activate`

_(Also accessible as `connector.activate(<WALLET_NAME>)`)_

Used when connecting a new wallet. If you have a `walletConnect` built from `WalletConnectConnector`:
```typescript
activate(walletConnect);
```
We'll describe connecting a wallet in more detail later.

#### `active`

A boolean showing if the user has a connected wallet, useful in conditional rendering.
```typescript
if(active){
  ...
} // or:
active && <PaymentModal>
```
#### `chainId`

_(Also accessible as `connector.getChainId()`)_

Number representation of the `chainId` the user is connected to. Useful for only displaying modals when the user is connected to supported networks, or displaying an alert when they are not.

For a comprehensive list of chains (both production and test networks), see [ChainId Network](https://chainid.network).
```typescript
if(chainId === 1){
  return <PaymentModal>
} else {
  return <>Please connect to Ethereum's Mainnet</>
}
```

#### `deactivate`

_(Also accessible as `connector.deactivate()`)_

Deactivate disconnects the wallet from the app. It is important to note that if the wallet has injected an iFrame (like Portis, for example), it will not be removed from the DOM, which can lead to issues if the user tries to reconnect. In these cases, it is better to use `connector.close()`. We'll discuss disconnecting the user separately.
```typescript
const disconnect = () => (
  <button onClick={deactivate}>Disconnect</button>
)
```
#### `error`

The `error` library exposes a number of potential errors that it recognizes. It does not stop execution when they are triggered, but rather gives the architect room to catch each error and customize how the applicaiton should react to it.

Most of these errors vary from wallet to wallet, and unfortunately not all are documented. We'll give one example to give an idea of how they work and then list errors from the supported wallets (MetaMask/injected providers, Portis, and Torus).

Most dApps support some chains (generally mainnet and some testnets), but not all. Properly identifying if the user is on a supported chain is a crticial part of good UX in a dApp. `web3-react` exposes a `UnsupportedChainIdError` that allows you to detect when a user is on an unsupported chain.

This error relies on an array of supported chains being supplied when the wallet is instantiated. You may recall from the WalletConnect example above that `supportedChainIds` is an optional argument, an array of numbers. This, or something like it, is either a mandatory or optional argument for every connector/wallet that `web3-react` recognizes. This example assumes you've passed in an array with supported chains.

First, import the error:
```typescript
import { UnsupportedChainIdError } from '@web3-react/core';
```
Then destructure it from the `useWeb3React` object:
```typescript
const {
  // whichever other web3-react libraries you need,
  chainId,
  error
} = useWeb3React();
```
Now if you wanted to have a specific reaction in the app, you can now use it. (Remember that `supportedChainIds` is an optional argument.) Here's one example:
```typescript
if(Boolean(connector.supportedChainIds) && !connector.supportedChainIds.includes(chainId)) {
  throw new UnsupportedChainIdError(chainId, connector.supportedChainIds);
}
```
The other globally available error is `StaleConnectorError`.

There are also custom errors depending on your wallet. We'll list them wallet by wallet for the supported wallets and our WalletConnect example.

**MetaMask/Injected Connector:**

 - `NoEthereumProviderError`: used when an injected provider is expected to be there but isn't ([example](https://github.com/NoahZinsmeister/web3-react/blob/v6/packages/injected-connector/src/index.ts#L70))
 - `UserRejectedRequestError`: used if the user rejects a transaction through the injected provider

**Portis** and **Torus** both do not have any custom errors.

**WalletConnect**

 - `UserRejectedRequestError`: same as in the injected connector

#### `provider`

_(Also accessible as `connector.getProvider()`)_

This is the same as the concept of a provider in web3.js. It also contains a signer unlike the Ethers paradigm where the provider and signer are two separated entities. This means that when using web3, the `provider` is passed in at contract instantiation, but with Ethers a simple provider (for example, using `ethers.getDefaultProvider()`) is sufficient for instantiating a contract, but the `web3react` provider will need to be connected to send transactions. (This might be a bit confusing. There is a dedicated section for working with contracts in Warp Core later.)
```typescript
myContract.connect(provider).myFunction{ value: 1 }(); //ethers
myContract = new web3.Contract(abi, address, provider); // web3
```
#### `setError`

`setError` takes an `Error` as an argument. `Error` is a built-in TypeScript type:
```typescript
interface Error {
    name: string;
    message: string;
    stack?: string;
}
```
When called, it reloads `web3react`. This is used internally in the library when errors happen - `web3react` will call `setError` and try to reload. You can also use this manually if you'd like to create a custom error which will then be available throughout the project.

You can set a new error type like this

```typescript
const { setError } = useWeb3React();

setError({ name: "fooError", message: "you got a foo error"});
```
Then you can use that error anywhere in the app:
```typescript
const { error } = useWeb3React();

if(error && error.name === "fooError"){
  doSomething();
}
```

#### Custom functions

In addition to the global functions above, some wallets (connectors) have their own unique functions exposed by `web3react`. These can be very important. `close()`, for example, will fully clear DOM-based wallets from the DOM, which `deactivate()` will not. (We'll cover disconnecting wallets in more detail below.)

If you'd like to find the custom functions for a given wallet, as of this writing you will need to actually look through the code of the connector to see what's there. The directory of all the connectors is [here](https://github.com/NoahZinsmeister/web3-react/tree/v6/packages). For example, if we're looking to see if there are any special functions for WalletConnect, we'd go to the [WalletConnect connector](https://github.com/NoahZinsmeister/web3-react/blob/v6/packages/walletconnect-connector/src/index.ts), where we'd see the global (public) functions (`activate`, `getProvider`, `getChainId`, `getAccount`, `deactivate`), and then also a [`close`](https://github.com/NoahZinsmeister/web3-react/blob/v6/packages/walletconnect-connector/src/index.ts#L126) function.

Of all the Warp Core supported wallets (MetaMask/Injected, Portis, Torus), and the WalletConnect example, Portis, Torus, and WaletConnect all have a `close()` function. We'll talk about `close()` a bit more when we talk about disconnecting wallets later. The only other custom function is `changeNetwork` for Portis, which allows you to change the chain Portis is connected to.

### Connecting with the Wallet

As mentioned before, the `handleConnect` function is what activates a wallet, It takes one argument, namely the connector object for the wallet being connected. We walked through making a `selectWalletConnect` function before. What this does is pass the `walletConnect` connector object into the state, which can then passed into `handleConnect`. (The actual connection is fired in `src/components/walletModal/walletModal.tsx`.) `handleConnect` instantiates the wallet by calling `web3-react`'s `activate` function on it.

In addition, Warp Core will reload the contracts once the wallet is connected. This allows more personalized information to be relayed. We'll talk more about the contract booter later.
### Disconnecting a Wallet

We've mentioned `deactivate` and `close` before. Unfortunately, it can be hard to know which to call when. In addition, MetaMask and the injected connectors do not have a `close` function, which can cause an error if it is called, and some wallets will not be cleared from the DOM _unless_  `close` is called, meaning that you cannot rely on `deactivate`. As a result, Warp Core implements a generic `disconnectWallet` function (in `src/containers/WalletWrapper.tsx`) which basically ensures that the right functions are called for disconnecting the wallet fully.

### Custom Warp Core Wallet UI Flags

Warp Core also provides some additional UI-related functions. You can leverage these by adding an object ot the `wallets` array in `src/containers/WalletWrapper.tsx`. Here is an example wallet:
```typescript
{
    name: "metamask",
    connectFunction: selectMetamask,
    selected: wallet === injectedConnector,
    activating: activatingConnector === injectedConnector,
    active: connector === injectedConnector,
    icon: Metamask,
},
```
We'll step through the different properties here. First let's briefly talk about the more self-explanatory ones:

* `name`: the `web3-react` name of the wallet (should be in `wallets/connectors.tsx` already)
* `connectFunction`: the name of the function for connecting this wallet in `wallets/connectors.tsx`, follows the pattern of `selectWALLET_NAME`
* `icon`: the media asset for the icon. This should be an svg stored in `components/icons`.

The other three are provided as UI flags, as mentioned above. You've likely noticed the general pattern of clicking a "select wallet" screen, being presented with the different wallets you can pick from, selecting one, then having the website connect with the wallet. What information can and should be displayed to a user often changes depending on what step along the process the user is at. These flags help Warp Core know what stage of the process the wallet connection is at to make the process of displaying the right components simpler.

These flags assume the user is being presented with a modal that contains wallet options, and that once they click on an option, that the wallet is not connected until they click on a "confirm" button (or similar).

* `selected`:  when the user has selected the wallet, but not yet confirmed
* `activating`: the "spinner state" - the wallet is in the process of being connected
* `active`: the wallet is connected

### Additional Utilities

MetaMask works by injecting itself into the browser window, but is not the only wallet that does so. We've had need to check if the injected wallet is MetaMask, so we made a MetaMask Checker. It lives in `src/wallets/utils.ts`, and is called `isMetamaskEnabled`. It works simply, and returns a boolean representing if the connected wallet is Metamask or not.

## Working With Contracts

TODO: loader, reloading contracts with a signer

// const web3 = new Web3(provider);
// updateContractsWeb3(web3);