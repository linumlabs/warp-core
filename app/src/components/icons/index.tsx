import { ReactComponent as DAI } from "../../media/tokens/dai.svg";
import { ReactComponent as ETH } from "../../media/tokens/ETH.svg";
import { ReactComponent as USDC } from "../../media/tokens/USDC.svg";
import { ReactComponent as WBTC } from "../../media/tokens/wBTC.svg";
import { ReactComponent as USDT } from "../../media/tokens/USDT.svg";
import { ReactComponent as Metamask } from "../../media/wallets/metamask.svg";
import { ReactComponent as Portis } from "../../media/wallets/portis.svg";
import { ReactComponent as Check } from "../../media/UI/check_circle.svg";
import { ReactComponent as Linum } from "../../media/branding/linum_labs.svg";
import { ReactComponent as TorusLight } from "../../media/wallets/torus_light.svg";

export const wallets = {
  metamask: Metamask,
  portis: Portis,
  torusLight: TorusLight
};
export const tokens = {
  dai: DAI,
  eth: ETH,
  usdc: USDC,
  wbtc: WBTC,
  usdt: USDT,
};

export const ui = {
  check: Check,
};

export const branding = {
  linum: Linum,
};

export { Metamask, Portis, DAI, ETH, USDC, WBTC, USDT, Check, Linum, TorusLight };
