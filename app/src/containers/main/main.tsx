import React from "react";
import WalletModal from "../../components/walletModal/walletModal";
import Navbar from "../navbar/navbar";
import useStyles from "./mainStyles";

export interface Props {
  wallet: any;
  showWalletModal: boolean;
  walletConnected: boolean;
  toggleWalletModal: () => void;
  disconnectWallet: (wallet: any) => void;
  setShowWalletModal: React.Dispatch<React.SetStateAction<boolean>>;
  handleConnect: (wallet: any) => void;
  canConnect: boolean;
  activatingConnector: any;
  chainIdIsCorrect: boolean;
  wallets: {
    name: string;
    connectFunction: () => void;
    selected: boolean;
    activating: boolean;
    active: boolean;
    icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  }[];
}

export default function Main(props: Props) {
  const classes = useStyles();
  return (
    <div className={classes.Main}>
      <Navbar setShowWalletModal={props.setShowWalletModal} />
      {props.showWalletModal && (
        <WalletModal
          wallet={props.wallet}
          setShowWalletModal={props.setShowWalletModal}
          handleConnect={props.handleConnect}
          canConnect={props.canConnect}
          activatingConnector={props.activatingConnector}
          wallets={props.wallets}
          disconnectWallet={props.disconnectWallet}
        />
      )}
    </div>
  );
}
