import React, { useContext, useState, useEffect } from "react";
import { StoreContext } from "../store/store";
import { isMetamaskEnabled } from "../wallets/utils";
import { useWeb3React } from "@web3-react/core";
import { useToggle } from "../hooks";
import { injectedConnector, portis, torus } from "../wallets/connectors";
import { Metamask, Portis, TorusLight } from "../components/icons";
import Web3 from "web3";
import Main from "./main/main";
import { ContractContext } from "../store/contractContext/contractContext";

export default function WalletWrapper() {
  const { actions } = useContext(StoreContext);
  const { updateContractsWeb3 } = useContext(ContractContext);
  const [showWalletModal, setShowWalletModal, toggleWalletModal] = useToggle(
    false
  );
  const [wallet, setWallet] = useState<any>(undefined);
  const [metamaskEnabled, setMetamaskEnabled] = useState(false);
  const [activatingConnector, setActivatingConnector] = useState<any>();
  const {
    connector,
    activate,
    deactivate,
    active,
    chainId,
    account,
  } = useWeb3React();

  useEffect(() => {
    const enabled = isMetamaskEnabled();
    setMetamaskEnabled(enabled);
  }, []);

  useEffect(() => {
    if (account) {
      actions.setWalletAddress(account);
    }
    // eslint-disable-next-line
  }, [account]);

  // ? if you want the modal to close when there is a completed connection, use this useEffect below
  // useEffect(() => {
  //   if (activatingConnector && activatingConnector === connector) {
  //     setActivatingConnector(undefined);
  //   }
  //   active && setShowWalletModal(false);
  // }, [activatingConnector, connector, active, setShowWalletModal]);



  const selectTorus = () => {
    setWallet(torus);
    // handleConnect(torus);
  };

  const selectMetamask = () => {
    setWallet(injectedConnector);
    // handleConnect(injectedConnector);
  };

  const selectPortis = () => {
    setWallet(portis);
    // handleConnect(portis);
  };

  const disconnectWallet = async (activeWallet: any) => {
    deactivate();
    setActivatingConnector(undefined);
    activeWallet !== injectedConnector && await activeWallet.close();
    actions.setWalletDisconnected();
  };

  const handleConnect = async (activatingWallet: any) => {
    try {
      setActivatingConnector(activatingWallet);
      let web3 = new Web3(Web3.givenProvider);
      await activate(activatingWallet);
      let { provider } = await activatingWallet.activate();
      web3 = new Web3(provider);
      actions.setProvider(provider);
      actions.setWalletConnected();
      setActivatingConnector(null);
      updateContractsWeb3(web3);

    } catch (err) {
      // ? If user closes wallet popup modal, rejects connection
      disconnectWallet(activatingWallet);
    }
  };

  const wallets = [
    {
      name: "metamask",
      connectFunction: selectMetamask,
      selected: wallet === injectedConnector,
      activating: activatingConnector === injectedConnector,
      active: connector === injectedConnector,
      icon: Metamask,
    },
    {
      name: "portis",
      connectFunction: selectPortis,
      selected: wallet === portis,
      activating: activatingConnector === portis,
      active: connector === portis,
      icon: Portis,
    },
    {
      name: "torus",
      connectFunction: selectTorus,
      selected: wallet === torus,
      activating: activatingConnector === torus,
      active: connector === torus,
      icon: TorusLight,
    },
  ];

  const enabledWallets = !metamaskEnabled
    ? wallets.filter((wallet) => wallet.name !== "metamask")
    : wallets;

  const chainIdIsCorrect =
    chainId && chainId.toString() === process.env.REACT_APP_CHAIN_ID;

  return (
    <div className="Main">
      <Main
        showWalletModal={showWalletModal}
        setShowWalletModal={setShowWalletModal}
        walletConnected={active}
        toggleWalletModal={toggleWalletModal}
        disconnectWallet={disconnectWallet}
        wallets={enabledWallets}
        canConnect={wallet && true}
        handleConnect={handleConnect}
        activatingConnector={activatingConnector}
        chainIdIsCorrect={chainIdIsCorrect}
        wallet={wallet}
      />
    </div>
  );
}
