import React, { useContext } from "react";
import { ThemeContext } from "../../store/themeContext/themeContext";
import useStyles from "./navbarStyles";
import { Linum } from "../../components/icons";

export interface Props {
  setShowWalletModal: React.Dispatch<React.SetStateAction<boolean>>;
}

function Navbar(props: Props) {
  const { theme } = useContext(ThemeContext);

  const classes = useStyles({ ...props, ...theme });

  return (
    <div className={classes.Navbar}>
      <Linum className={classes.logo} />
      <button
        className={classes.walletConnectButton}
        onClick={() => props.setShowWalletModal(true)}
      >
        Open Wallet Modal
      </button>
    </div>
  );
}

export default React.memo(Navbar);
